package mongo1

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
	"n_auth/pkg/models"
	"time"
)

type UserModel struct {
	Database *mongo.Client
}

// adds new registered user for Database
func (m *UserModel) InsertUser(newUser models.User) error {
	//connects to Database collection
	collection := m.Database.Database("users").Collection("users_data")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	// checks for duplicated emails
	err := collection.FindOne(ctx, bson.M{"email": newUser.Email})
	if err.Err() != mongo.ErrNoDocuments {
		return errors.New("SAME EMAIL")
	}

	hashedPassword, err1 := bcrypt.GenerateFromPassword([]byte(newUser.Password), 2)
	if err1 != nil {
		return err1
	}

	newUser.Password = string(hashedPassword)

	_, errr := collection.InsertOne(ctx, newUser)
	if errr != nil {
		return errr
	}
	return nil
}

func (m *UserModel) Auth(newUser models.User) (string, error) {
	var user models.User
	collection := m.Database.Database("users").Collection("users_data")
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)

	err := collection.FindOne(ctx, bson.M{"email": newUser.Email})
	if err.Err() == mongo.ErrNoDocuments {
		return "", errors.New("NOT REGISTERED")
	}

	decodeErr := err.Decode(&user)
	if decodeErr != nil {
		return "", decodeErr
	}

	newErr := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(newUser.Password))
	if newErr != nil {
		if errors.Is(newErr, bcrypt.ErrMismatchedHashAndPassword) {
			return "", errors.New("INVALID CREDENTIALS")
		} else {
			return "", newErr
		}
	}

	return newUser.Email, nil
}
