package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"n_auth/pkg/models"
	"n_auth/pkg/models/mongo1"
	"net/http"
	"os"
	"time"
)

// secret string for jwt
var secret string

// func for generation of token
func GetToken(user_email string) (string, error) {
	fmt.Println(user_email)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_email": user_email,
	})
	fmt.Println(secret)
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// handler for authentication of users and sending jwt token
func (app *application) loginUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not Allowed", http.StatusBadRequest)
	} else {
		var user models.User
		errr := json.NewDecoder(r.Body).Decode(&user)
		if errr != nil {
			http.Error(w, "INVALID DATA"+errr.Error(), http.StatusBadRequest)
			return
		}

		user1, err := app.users.Auth(user)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(err.Error())
			return
		} else {
			fmt.Println(user1)
			token, err := GetToken(user1)
			if err != nil {
				log.Fatal(err)
			}
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(token)
		}
	}
}

// handler for registration of users
func (app *application) registerUser(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		http.Error(w, "Method not Allowed", http.StatusBadRequest)
	} else {
		var user models.User

		errr := json.NewDecoder(r.Body).Decode(&user)
		if errr != nil {
			http.Error(w, "INVALID DATA"+errr.Error(), http.StatusBadRequest)
			return
		}

		if user.Email == "" {
			http.Error(w, "EMAIL IS NOT PASSED", http.StatusBadRequest)
			return
		}

		if user.Password == "" {

			http.Error(w, "PASSWORD IS NOT PASSED", http.StatusBadRequest)
			return
		}

		err := app.users.InsertUser(user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} else {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode("registration was successful")
		}
	}

}

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger
	users    *mongo1.UserModel
}

func (app *application) routes() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/login", app.loginUser)
	mux.HandleFunc("/register", app.registerUser)

	return mux
}

func main() {

	// get port and secret for jwt
	addr := flag.String("addr", ":8000", "HTTP network address")
	secret = *flag.String("secret", "naruto_uzumaki", "secret_for_jwt")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	// we passed container name in order to work with docker
	// actually here had to be "localhost", but we pass "CONTAINER NAME! -> mongodb" and add depends_on param in docker-compose
	clientOptions := options.Client().ApplyURI("mongodb://mongodb:27017")
	client1, _ := mongo.Connect(ctx, clientOptions)

	// architecture from book from first half
	app := &application{
		errorLog: errorLog,
		infoLog:  infoLog,
		users:    &mongo1.UserModel{Database: client1},
	}

	srv := &http.Server{
		Addr:         *addr,
		ErrorLog:     errorLog,
		Handler:      app.routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	infoLog.Printf("Starting server on %s", *addr)
	err := srv.ListenAndServe()
	errorLog.Fatal(err)
}
